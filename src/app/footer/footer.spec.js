import angular from 'angular';
import 'angular-mocks';
import {footer} from './footer';

describe('footer component', () => {
  beforeEach(() => {
    angular
      .module('aimiaFooter', ['app/footer/footer.html'])
      .component('aimiaFooter', footer);
    angular.mock.module('aimiaFooter');
  });

  it('should credit \'ICO\'', angular.mock.inject(($rootScope, $compile) => {
    const element = $compile('<aimia-footer></aimia-footer>')($rootScope);
    $rootScope.$digest();
    const footer = element.find('a');
    expect(footer.html().trim()).toEqual('ICO');
  }));
});
