import angular from 'angular';
import 'angular-mocks';
import {header} from './header';

describe('header component', () => {
  beforeEach(() => {
    angular
      .module('aimiaHeader', ['app/header/header.html'])
      .component('aimiaHeader', header);
    angular.mock.module('aimiaHeader');
  });

  it('should render \'Aimia ♥ Coffee\'', angular.mock.inject(($rootScope, $compile) => {
    const element = $compile('<aimia-header></aimia-header>')($rootScope);
    $rootScope.$digest();
    const header = element.find('h2');
    expect(header.html().trim()).toEqual('Aimia ♥ Coffee');
  }));
});
