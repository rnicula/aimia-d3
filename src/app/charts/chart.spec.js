import angular from 'angular';
import 'angular-mocks';
import {chart} from './chart';

describe('chart component', () => {
  beforeEach(() => {
    angular
      .module('aimiaChart', ['app/charts/chart.html'])
      .component('aimiaChart', chart);
    angular.mock.module('aimiaChart');
  });

  it('should render Total Coffee Consumption (2014)', angular.mock.inject(($rootScope, $compile) => {
    const $scope = $rootScope.$new();
    $scope.fixture = {
      title: "Total Coffee Consumption (2014)",
      data: [
        {
          color: "rgb(82, 179, 126)",
          values: [
            {
              label: "United States",
              value: 23800000
            },
            {
              label: "Canada",
              value: 3900000
            },
            {
              label: "United Kingdom",
              value: 3600000
            }
          ]
        }
      ]
    };

    const element = $compile('<aimia-chart chart="fixture"></aimia-chart>')($scope);
    $scope.$digest();
    const chart = element.find('md-card-title-text');
    expect(chart.text().trim()).toEqual('Total Coffee Consumption (2014)');
  }));
});
