import angular from 'angular';

import {chart} from './chart';
import {charts} from './charts';

export const chartsModule = 'charts';

angular
  .module(chartsModule, [])
  .component('aimiaChart', chart)
  .component('aimiaCharts', charts);
