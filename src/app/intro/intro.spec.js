import angular from 'angular';
import 'angular-mocks';
import {intro} from './intro';

describe('intro component', () => {
  beforeEach(() => {
    angular
      .module('aimiaIntro', ['app/intro/intro.html'])
      .component('aimiaIntro', intro);
    angular.mock.module('aimiaIntro');
  });

  it('should render \'Gotta love coffee...\'', angular.mock.inject(($rootScope, $compile) => {
    const element = $compile('<aimia-intro></aimia-intro>')($rootScope);
    $rootScope.$digest();
    const intro = element.find('h1');
    expect(intro.html().trim()).toEqual('Gotta love coffee...');
  }));
});
