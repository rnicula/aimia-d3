# Aimia3

## Angular Coffee Stats page using angular 1.x, angular-nvd3 and angular-material
## Author: Diana Nicula
## Time: 2 hours

![Aimia3 Screenshot](/screenshot.png?raw=true)

### Dependencies

Run `npm install` and `bower install` to install dependencies.

### Development server

Run `gulp serve` for a dev server.

### Building

Run `gulp build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `gulp test` to execute the unit tests.
